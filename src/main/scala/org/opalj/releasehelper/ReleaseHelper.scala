/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2017
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.releasehelper

import sbt.{AutoPlugin, Def, _}
import sbt.Keys._
import java.io.File

import org.opalj.releasehelper.transport.Transport

import scala.util.Try


/**
  * Plugin to help with the release process of OPAL.
  * In this object there are all keys and settings that have been modified from the base plugin "sbt-release"
  *
  * @author Simon Leischnig
  */
object ReleaseHelperPlugin extends AutoPlugin {

  object autoImport {
    // tasks relating to the upload of the webpage
    val rhWebpageLocalDirectory = taskKey[File]("the local base directory of the webpage")
    val rhWebpageRemoteWWWPath = taskKey[String]("the name of the www directory on the remote server inside its configured base path")
    val rhWebpageUploadConfigFile = taskKey[File]("Upload config file incl. login data for the website")
    val rhWebpageUpload = taskKey[Unit]("Upload task for the website")
  }

  import autoImport._

  val copyLocalConfigFileCmd = Command.single("rhConfigTemplate"){ case (state, fileName) =>
    val replaced = fileName.replaceFirst("^~",System.getProperty("user.home"))
    if(! file(replaced).isAbsolute) {
      sys.error(s"The specified path $fileName is relative; Placing the ftp cofiguration into the git repository is not recommended, thus we require you to specify an absolute path.")
    }
    if(file(replaced).exists()) {
      sys.error(s"The specified file $replaced already exists; please delete an old file manually before replacing it.")
    }

    Try{
      val content = """ftp {
                      |  opalhome.host = "ftp host URL as string"
                      |  opalhome.port = 21
                      |  opalhome.user = "your user name"
                      |  opalhome.password = "your password"
                      |}
                      |""".stripMargin
      IO.write(file(replaced), content)
      println(s"A template file for FTP transport was created at $replaced")
    }.recover{case t =>
      println(s"The ftp configuration template could not be created: ${t.getMessage}.")
    }

    state
  }

  /*
  Convenience settings and tasks to be used by the OPAL root project
   */
  lazy val opalBaseProjectConvenienceSettings: Seq[Def.Setting[_]] = Seq(

  rhWebpageLocalDirectory := {sys.error("local webpage directory key 'rhWebpageLocalDirectory' not set in the build settings! It can be set by putting 'rhWebpageLocalDirectory := file(\"/path/to/local/directory\")' in your global sbt build settings, e. g. in project/FTPUpload.scala.")},
  rhWebpageRemoteWWWPath := {sys.error("remote webpage directory path 'rhWebpageRemoteWWWPath' not set in the build settings! It can be set by putting 'rhWebpageRemoteWWWPath := \"/path/to/remote/www/dir\"' in your global sbt build settings, e. g. in project/FTPUpload.scala.")},
    rhWebpageUploadConfigFile := {sys.error("There is no ftp configuration file set. " +
      "You may use the command 'rhConfigTemplate <config-file-path>' to generate a template file for it. After " +
      "filling in the blanks there, set it via 'rhWebpageUploadConfigFile := file(\"<config-file-path>\")' in your sbt build definition.")},
    rhWebpageUpload := {
      val cfgFile = rhWebpageUploadConfigFile.value
      val localWWWDir = rhWebpageLocalDirectory.value
      val remoteWWWPath = rhWebpageRemoteWWWPath.value
      if(! cfgFile.exists()) {
        sys.error(s"The config file for the FTP transport of the OPAL website has not been found: $cfgFile. To be specified in taskKey 'rhWebpageUploadConfigFile'")
      }
      if(!localWWWDir.exists() || !localWWWDir.isDirectory || localWWWDir.listFiles.size == 0) {
        sys.error(s"The specified local WWW directory $localWWWDir is nonexistent or empty")
      }
      println(s"Website upload initiating: ($localWWWDir -> $remoteWWWPath)")
      val transportResult = Utils.obtainOpalWebsiteTransport(cfgFile).withEstablished{ conn =>
        conn.uploadDirectory(localWWWDir, remoteWWWPath)
      }.flatten
      transportResult.recoverWith{case t:Throwable => Try {
        println(s"could not complete FTP operation because the following error occured. Maybe check your ftp config file?")
        throw t
      }}.get
    },
    commands += copyLocalConfigFileCmd

  )

  override def projectSettings: Seq[Def.Setting[_]] = opalBaseProjectConvenienceSettings

  // Fragen
  /*
    - git checks: currently: no untracked files, no unstaged files
    - form of version changes; Proposal:
      # OPAL-sbt: externalize version into /version.sbt
      # OPAL-sbt-external-projects: externalize dependency version into opal-depend-version.sbt
      # textual files: match invisible markers around
    -
  */

}
