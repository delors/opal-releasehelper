// For information on how to use this plugin, see the accompanying Readme.md document.
scalaVersion := "2.12.4"
version  := "0.45-SNAPSHOT"
organization := "de.opal-project"
licenses += ("BSC 2-clause", url("https://opensource.org/licenses/BSD-2-Clause"))

scalacOptions in ThisBuild ++= Seq("-feature", "-deprecation")

sbtPlugin := true

publishMavenStyle := false

name := "opal-releasehelper"
description := "Provides automations for creating new releases of OPAL"

resolvers += "novus repo" at "http://repo.novus.com/releases/"

libraryDependencies ++= Seq("org.scalatest" %% "scalatest" % "3.0.3" % "test")
libraryDependencies += "commons-net" % "commons-net" % "3.6"
libraryDependencies += "com.typesafe" % "config" % "1.3.1"