# OPAL-releasehelper
SBT plugin for aiding the release process, specifically by providing an upload task for the generated website.

## 1. Add the plugin to your project

In your sbt project's ``project/plugins.sbt``, add:
```
lazy val root = (project in file(".")) dependsOn (rhplugin)
lazy val rhplugin = RootProject(uri("https://bitbucket.org/delors/opal-releasehelper.git#8ea70f110ec8073ae00d416e358bf37eb1ae1203"))
addSbtPlugin("de.opal-project" % "opal-releasehelper" % "0.45-SNAPSHOT")
```

## 2. Set the build-level settings

The following keys have to be set on the build-level. Please note, that in the OPAL project, the file ``project/FTPUpload.scala`` already sketch the definition of those settings for easy reference.
 
    rhWebpageLocalDirectory -- of type File - the local base directory of the webpage"). OPAL has this setting already set in ``project/FTPUpload.scala``
    rhWebpageRemoteWWWPath -- of type String - e.g. "/www", the name of the www directory on the remote server inside its configured base path")
    rhWebpageUploadConfigFile -- of type File - Upload config file incl. login data for the website")
    
In ``.scala`` files, you may need to ``import org.opalj.releasehelper.ReleaseHelperPlugin.autoImport._`` to access those keys.    
    
Where the setting ``rhWebpageUploadConfigFile`` refers to a file that stores the ftp credentials, URL and port. 
As a convencience method to create such a file, open up your sbt shell and execute the 
command ``rhConfigTemplate <path/to/configfile.cfg>``, or create it yourself with the content: 

    ftp {
      opalhome.host = "ftp host URL as string"
      opalhome.port = 21
      opalhome.user = "your user name"
      opalhome.password = "your password"
    }
    
Adapt this content to your needs and set the configuration file in your build by putting something like

```
rhWebpageUploadConfigFile := file("/home/simon/test.cfg")
```

The configuration file contains sensitive data, so don't put it in your git-managed directory.

Finally, enable the plugin in your build.sbt file, as so:

```
enablePlugins(ReleaseHelperPlugin)
```
    
## 3 Usage: 

Plain and simple: 

Use the task val ``rhWebpageUpload`` in the sbt shell, or use ``sbt rhWebpageUpload`` on the command line, to perform uploading the website.

You can also use the task like any other in your build definition to depend on by putting

```
otherTaskKey := {
    val upload = rhWebpageUploadConfigFile.value
    // other code
}
```